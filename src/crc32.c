
#include <stdlib.h>
#include <stdio.h>

#ifndef _POLY_
#define poly 0x5B84A5C0L
/* #define poly 0x82608EDBL */
/* #define poly 0x04C11DB7L */
/* #define poly 0xEDB88320L */
/* #define poly reverse32(0x82608EDBL) */
/* #define poly reverse32(0x04C11DB7L) */
/* #define poly reverse32(0xEDB88320L) */
// #define poly 0xDB710641L // == reverse(0x04C11DB7L)
#endif

typedef unsigned int  uint32;
typedef unsigned char uint8;
// typedef (unsigned long long) uint64;
uint32 reverse32(uint32 );
uint8  reverse08(uint8  );
uint32 crc32(uint8 *, int );

int main()
{
    uint32 crc;
    uint8*  message = (uint8 *)calloc(58 , sizeof(uint8) );
    message[ 0] = 0xFF,
    message[ 1] = 0xFF,
    message[ 2] = 0xFF,
    message[ 3] = 0xFF,
    message[ 4] = 0xFF,
    message[ 5] = 0xFF,
    message[ 6] = 0x00,
    message[ 7] = 0x11,
    message[ 8] = 0x22,
    message[ 9] = 0x33,
    message[10] = 0x44,
    message[11] = 0x55,
    message[12] = 0x08,
    message[13] = 0x00,
    message[14] = 0x45,
    message[15] = 0x3F,
    message[16] = 0x00,
    message[17] = 0x2E,
    message[18] = 0xB3,
    message[19] = 0xFE,
    message[20] = 0x00,
    message[21] = 0x00,
    message[22] = 0x80,
    message[23] = 0x11,
    message[24] = 0x00,
    message[25] = 0x00,
    message[26] = 0xC0,
    message[27] = 0xA8,
    message[28] = 0x64,
    message[29] = 0x67,
    message[30] = 0xC0,
    message[31] = 0xA8,
    message[32] = 0x64,
    message[33] = 0x68,
    message[34] = 0x23,
    message[35] = 0x8F,
    message[36] = 0x23,
    message[37] = 0x90,
    message[38] = 0x00,
    message[39] = 0x1A,
    message[40] = 0x00,
    message[41] = 0x00,
    message[42] = 0x46,
    message[43] = 0x00,
    message[44] = 0x00,
    message[45] = 0x00,
    message[46] = 0x00,
    message[47] = 0x00,
    message[48] = 0x00,
    message[49] = 0x00,
    message[50] = 0x00,
    message[51] = 0x00,
    message[52] = 0x00,
    message[53] = 0x00,
    message[54] = 0x00,
    message[55] = 0x00,
    message[56] = 0x00,
    message[57] = 0x00,
    /* message[ 0] = 0x31; */
    /* message[ 1] = 0x32; */
    /* message[ 2] = 0x33; */
    /* message[ 3] = 0x34; */
    /* message[ 4] = 0x35; */
    /* message[ 5] = 0x36; */
    /* message[ 6] = 0x37; */
    /* message[ 7] = 0x38; */
    /* message[ 8] = 0x39; */
    /* message[ 9] = 0xc0; */
    /* message[10] = 0x83; */
    /* message[11] = 0x14; */
    /* message[12] = 0x08; */
    /* message[13] = 0x06; */
    /* message[14] = 0x00; */
    /* message[15] = 0x01; */
    /* message[16] = 0x08; */
    /* message[17] = 0x00; */
    /* message[18] = 0x06; */
    /* message[19] = 0x04; */
    /* message[20] = 0x00; */
    /* message[21] = 0x02; */
    /* message[22] = 0xe8; */
    /* message[23] = 0x03; */
    /* message[24] = 0x9a; */
    /* message[25] = 0xc0; */
    /* message[26] = 0x83; */
    /* message[27] = 0x14; */
    /* message[28] = 0xc0; */
    /* message[29] = 0xa8; */
    /* message[30] = 0x01; */
    /* message[31] = 0x1c; */
    /* message[32] = 0x10; */
    /* message[33] = 0x7b; */
    /* message[34] = 0xef; */
    /* message[35] = 0x54; */
    /* message[36] = 0xb0; */
    /* message[37] = 0x98; */
    /* message[38] = 0xc0; */
    /* message[39] = 0xa8; */
    /* message[40] = 0x01; */
    /* message[41] = 0x01; */
    printf("message         : 0x");
    for(int i = 0; i < 58; i++)
        printf("%02x", (uint8) message[i]);
    printf("\n");
    crc = crc32(message, 58);
    uint32 o = poly;
    printf("poly            : 0x%08x; not poly            : 0x%08x\n", o, ~o);
    printf("reversed poly   : 0x%08x; not reversed poly   : 0x%08x\n", reverse32( o ), ~reverse32( o ) );
    printf("crc32           : 0x%08x; not crc32           : 0x%08x\n", crc, ~crc );
    printf("reversed crc32  : 0x%08x; not reversed crc32  : 0x%08x\n", reverse32(crc), ~reverse32(crc) );
    free(message);
    return 0;
}

/*
  Name  : CRC-32
  Poly  : 0x04C11DB7    x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11
                       + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x + 1
  Init  : 0xFFFFFFFF
  Revert: true
  XorOut: 0xFFFFFFFF
  Check : 0xCBF43926 ("123456789")
  Check : 0xA3830348 ("ABC")
  MaxLen: 268 435 455 байт (2 147 483 647 бит) - обнаружение
   одинарных, двойных, пакетных и всех нечетных ошибок
*/
uint32 crc32(uint8 *message, int len)
{
    uint32 crc_table[256];
    uint32 crc;
    int i, j;
 
    for (i = 0; i < 256; i++)
    {
        /* crc = 0xFF000000L & (i << 24); */
        crc = i;
        for (j = 0; j < 8; j++)
            /* crc = crc & 0x80000000L ? (crc << 1) ^ poly : crc << 1; */
            crc = ( crc & 1 )? (crc >> 1) ^ poly : crc >> 1;
 
        crc_table[i] = crc;
    };
    for ( i = 0; i < 16; i++ )
    {
      for ( j = 0; j < 16; j++ )
        printf( "%08x ", crc_table[i*16+j] );
      printf( "\n" );
    }
    printf( "\n" );
 
    crc = (uint32)0xFFFFFFFFL;
 
    while (len--)
    {
        printf( "len : %3d, crc32: %08x, crc32 xor -1: %08x\n", len, crc, ~crc );
        /* crc = crc_table[(crc ^ *message++) & 0xFF] ^ (crc << 8); */
        crc = crc_table[(crc ^ *message++) & 0xFF] ^ (crc >> 8);
    }
 
    return crc ^ (uint32)0xFFFFFFFFL;
}

uint32 reverse32(uint32 crc)
{
    uint32 crc_r = 0;
    int i;
    for(i = 0; i < 32; i++)
    {
        crc_r = crc_r << 1;
        if ( (crc & 0x0001) == 1)
            crc_r |= 1;
        crc   = crc >> 1;
    }
    return (uint32)crc_r;
}

uint8  reverse08(uint8  crc)
{
    uint8  crc_r = 0;
    int i;
    for(i = 0; i < 8; i++)
    {
        if ( (crc & 0x01) == 1)
            crc_r |= 1;
        crc_r = crc_r << 1;
        crc   = crc >> 1;
    }
    return crc_r;
}

#//CROSS_COMPILE = arm-angstrom-linux-gnueabi-
DEBUG         = 1
CC           := $(CROSS_COMPILE)gcc
ifeq ($(DEBUG), 1)
CC           := $(CC) -g
endif
CFLAGS        = -Wall -c
LDFLAGS       = -Wall 
LIBS          = 
LIBS_PATH     = -L"./libs" -L"/usr/lib" -L"/lib" 
INCLUDES      = 
INCL_PATH     = 

TOPDIR        = .
SRCDIR        = $(TOPDIR)/src
OBJDIR        = $(TOPDIR)/obj
EXEDIR        = $(TOPDIR)/release
SRCREL        = $(SRCDIR)/*.c $(SRCDIR)/*.cpp
SOURCE        = $(wildcard $(SRCREL))
SRC           = $(notdir $(SOURCE))
OBJ          := $(SRC:.c=.o)
OBJ          := $(OBJ:.cpp=.o)
ELFFILE       = crc.elf
OBJECT        = $(addprefix $(OBJDIR)/, $(OBJ))
ifeq ($(DEBUG),1)
ELFDIR        = $(TOPDIR)/debug
ELFEXE        = $(ELFDIR)/$(ELFFILE)
else
ELFDIR        = $(TOPDIR)/release
ELFEXE        = $(ELFDIR)/$(ELFFILE)
endif

all: elf

debug:
	gdb $(ELFEXE)

run:
	$(ELFEXE)

elf: $(OBJ)
	if ! [ -d $(ELFDIR) ]; then mkdir $(ELFDIR); cp libs/* $(ELFDIR);  fi
	$(CC) -o $(ELFEXE) $(OBJECT) $(LIBS_PATH) $(LIBS) 

VPATH := $(SRCDIR)

%.o:%.c
	@if ! [ -d $(OBJDIR) ]; then mkdir $(OBJDIR); fi
	$(CC) -c -o $(OBJDIR)/$@ $< $(CFLAGS) $(INCL_PATH) $(INCLUDES)

%.o:%.cpp
	@if ! [ -d $(OBJDIR) ]; then mkdir $(OBJDIR); fi
	echo $(OBJDIR)
	$(CC) -o $(OBJDIR)/$@ $< $(CFLAGS) $(INCL_PATH) $(INCLUDES)

test:
	@echo  TOPDIR = $(TOPDIR) 
	@echo  0 $(wildcard LIBS)
	@echo  1 $(LIBS) 
	@echo  2 $(LIBS_PATH) 
	@echo  3 $(INCLUDES) 
	@echo  4 $(INCL_PATH) 
	@echo  SRC    = $(SRC) 
	@echo  SRCREL = $(SRCREL) 
	@echo  SOURCE = $(SOURCE) 
	@echo  OBJ    = $(OBJ) 
	@echo  OBJDIR = $(OBJDIR) 
	@echo  OBJECT = $(OBJECT) 
	@echo  CC     = $(CC)
	@echo  ELFEXE = $(ELFEXE)

.PHONY: clean debug

clean:
	rm -rf obj/
	rm -rf release/*
	rm -rf debug/*

